#!/bin/bash

source /opt/intel/ipp/bin/ippvars.sh -arch intel64
icc -lcrypto -O3 -lpthread phrase.c -o phrase
icc -O3 -lpthread -ipp=crypto phrase-intel.c -o phrase-intel
