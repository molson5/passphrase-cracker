#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
//#include <openssl/sha.h>
#include <ippcp.h>

unsigned long dict_size; /* Number of words in wordlist */
char *word_sizes; /* Stores the size of each word in dict */
char **dict; /* Stores each word in the wordlist */
pthread_t *threads;
unsigned long **ranges; /* Two values per numthreads, start and end */
unsigned long num_words;
unsigned char hash[] = {0xeb, 0xbe, 0x5b, 0x8b, 0xad, 0x4a, 0x1d, 0x88,
                        0x26, 0x1f, 0x88, 0xc1, 0x9b, 0xee, 0xcb, 0x2a,
                        0xb1, 0x19, 0xe2, 0x30, 0xf6, 0x9d, 0x7c, 0x6c,
                        0xbd, 0x4f, 0x5a, 0xa1, 0x1a, 0xf9, 0x7f, 0x7e};
unsigned char group_name[] = "TheLimpDiskettes:";
unsigned char challenge[] = "946113317:";

void choose(unsigned long more, unsigned long *chosen, char *visited)
{
  unsigned long i;
  if(more == 0) {
    IppsSHA256State *context;
    unsigned char sha[32];
    ippsSHA256Init(context);
    ippsSHA256Update(group_name, 17, context);
    ippsSHA256Update(challenge, 10, context);
    for(i = 0; i < num_words; i++) {
      ippsSHA256Update(dict[chosen[i]], word_sizes[chosen[i]], context);
    }
    ippsSHA256Final(sha, context);
    for(i = 0; i < 32; i++) {
      if(sha[i] != hash[i]) return;
    }
    printf("FOUND IT:\n");
    for(i = 0; i < num_words; i++) {
      printf("%s", dict[chosen[i]]);
    }
    printf("\n");
    fflush(stdout);
    exit(0);
  }
  
  for(i = 0; i < dict_size; i++) {
    if(visited[i]) continue;
    visited[i] = 1;
    chosen[more] = i;
    choose(more - 1, chosen, visited);
    visited[i] = 0;
  }
}

void *check_range(void *arg)
{
  unsigned long i, *chosen, *range = arg;
  char *visited;

  visited = calloc(dict_size, sizeof(char));
  chosen = malloc(sizeof(unsigned long) * num_words);

  for(i = range[0]; i <= range[1]; i++) {
    chosen[0] = i;
    visited[i] = 1;
    choose(num_words - 1, chosen, visited);
  }
}

int main(int argc, char **argv)
{
  FILE *file;
  unsigned long num_threads, words_per_thread, runoff, i;
  signed long long last;
  ssize_t bytes_read;
  size_t length;
  int retval;

  if(argc != 4) {
    fprintf(stderr, "USAGE: ./phrase [wordlist] [num_words] [num_threads]\n");
    exit(1);
  }

  /* Get the arguments */
  num_words = strtoul(argv[2], NULL, 10);
  num_threads = strtoul(argv[3], NULL, 10);

  file = fopen(argv[1], "r");
  if(!file) {
    fprintf(stderr, "Failed to open file. Aborting.\n");
    exit(1);
  }

  /* Read the wordlist into dict */
  dict_size = 0;
  length = 0;
  while(1) {
    dict = realloc(dict, sizeof(char *) * (dict_size + 1));
    word_sizes = realloc(word_sizes, sizeof(char) * (dict_size + 1));
    dict[dict_size] = NULL;
    bytes_read = getline(&(dict[dict_size]), &length, file);
    if(bytes_read == -1) {
      free(dict[dict_size]);
      break;
    }
    word_sizes[dict_size] = bytes_read - 1;
    dict[dict_size][bytes_read - 1] = 0;
    dict_size++;
  }

  /* Abort if empty wordlist */
  if(dict_size == 0) {
    fprintf(stderr, "No words in wordlist. Aborting.\n");
    free(dict);
    fclose(file);
    exit(1);
  }

  /* Allocate the array of threads and arguments */
  threads = malloc(sizeof(pthread_t) * num_threads);
  ranges = malloc(sizeof(unsigned long *) * num_threads);

  /* Set up the arguments */
  words_per_thread = dict_size / num_threads;
  runoff = dict_size % num_threads; /* Number of threads that get one more */
  last = -1;
  for(i = 0; i < num_threads; i++) {
    ranges[i] = malloc(sizeof(unsigned long) * 2);

    ranges[i][0] = last + 1;
    if(i >= num_threads - runoff) { /* It's a runoff thread */
      ranges[i][1] = ranges[i][0] + words_per_thread;
    } else {
      ranges[i][1] = ranges[i][0] + words_per_thread - 1;
    }
    last = ranges[i][1];

    retval = pthread_create(&(threads[i]), NULL, check_range, (void *) ranges[i]);
  }

  /* Wait for the threads to finish up, clean up */
  for(i = 0; i < num_threads; i++) {
    pthread_join(threads[i], NULL);
    free(ranges[i]);
  }
  for(i = 0; i < dict_size; i++) {
    free(dict[i]);
  }
  free(ranges);
  free(threads);
  free(dict);
  fclose(file);
}
