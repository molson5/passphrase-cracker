This project is a little toy password cracker that's designed to take full advantage
of the parallelism offered by the Xeon Phi (Knight's Landing). A SHA256 is given to
us, with the original string in the form of "group_name:challenge:password."
The group name and challenge are given to us (rather, grabbed using Wireshark),
and the password is simply a concatenation of three of the words in the provided
dictionary. 

This program splits the dictionary throughout multiple threads, and generates
every possible password from a simply concatenation of the words in the dictionary.